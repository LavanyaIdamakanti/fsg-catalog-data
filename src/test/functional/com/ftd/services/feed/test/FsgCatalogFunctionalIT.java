package com.ftd.services.feed.test;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ftd.automation.framework.utilities.ExcelData;
import com.ftd.services.product.api.domain.response.ProductServiceResponse;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.*;

import com.ftd.automation.framework.base.RestAssuredBaseTest;
import com.ftd.automation.framework.report.Report;

public class FsgCatalogFunctionalIT extends RestAssuredBaseTest {

    private ProductService productService = new ProductService();

    /*@DataProvider(name = "TestData1")
    public static Object[][] testData() throws Exception {

        Object[][] arrayObject = ExcelData.getCombinedRowsData(
                "src/test/resources/prod1/Testdata.xlsx",
                0,1,102, "ref", getProp().getProperty("priority"));
        return arrayObject;
    }

    //FSG- Calyx db product data
    @Test(dataProvider = "TestData1")
    public void getProductResponseAndWriteToFile(HashMap<String,Object>[] productInfo) throws IOException {

        Report.createTest("ProductId - ");
        List<HashMap<String, Object>> productsMapList = new ArrayList<>();

        for(HashMap<String, Object> s : productInfo) {
            if(s != null && s.size() > 0) {
                productsMapList.add(s);
            }
        }

        List<String> productIdList=new ArrayList<>();
//        List<HashMap<String, Object>> collect =productsMapList.stream().filter(item -> item != null && !"".equals(item)).collect(Collectors.toList());
        productsMapList.forEach(productMap->{
            productIdList.add(productMap.get("Sku").toString().toUpperCase());
        });

        String productIds= StringUtils.join(productIdList, ",");
        Report.info("ProductIds-"+productIds);
        Report.info(
            "#########################################################");
        Report.info("*** Product information from service ***");
        ProductServiceResponse products = productService.getProductServiceResponse(productIds, getBrand(), getProp());
        productService.writeProductData(products,"src/test/resources/prod1/Testdata.xlsx", "ProductData", 1);
    }
*/

    //All ftd products data
    @Test(dataProvider = "DynamicTestData")
    public void getFTDProductWriteToFile(List<String> productInfo) throws IOException {

        Report.createTest("ProductId - ");
        //System.out.println(productInfo);

        String productIds= StringUtils.join(productInfo, ",");
        Report.info("ProductIds-"+productIds);
        Report.info(
                "#########################################################");
        Report.info("*** Product information from service ***");
        ProductServiceResponse products = productService.getProductServiceResponse(productIds, getBrand(), getProp());
        productService.writeFTDProductData(products,"src/test/resources/prod1/Testdata_ftd.xlsx", "Sheet2", 1);
    }

    @DataProvider(name = "DynamicTestData")
    public Object[] getAllProducts() throws IOException {
        String productCountString=getProp().getProperty("productCount");
        int numOfProducts;
        //List<String> combinedList = productService.getProducts(getBrand(), getProp(), false);
        List<String> activeProducts = productService.getAllProducts(getBrand(), getProp());
        System.out.println(activeProducts.size());
        List<String> productList = new ArrayList<>();
        List<List<String>> allProductsList = new ArrayList<>();

        int count=0;
        for(String s : activeProducts) {
            count++;
            productList.add(s);
            if(count>=100){
                allProductsList.add(productList);
                productList = new ArrayList<>();
                count=0;
            }
        }
        allProductsList.add(productList);
        Object[] allProdId=allProductsList.stream().toArray(Object[]::new);
        return allProdId;
    }
}
