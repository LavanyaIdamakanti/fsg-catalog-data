package com.ftd.services.feed.test;

import com.ftd.automation.framework.utilities.Log;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Utils {
    private static XSSFWorkbook workbook;
    private static XSSFSheet sheet;
    static int rowNumber = 0;
    public static void writeToExcel(@NonNull String filePath, String sheetName, List<List<String>> data) {
        log.info(
                "Writing data to column {} while excluding header row for headings from excel sheet located at {}",
                 sheetName, filePath);

        try {
            FileInputStream file = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(file);
            sheet=workbook.getSheet(sheetName);
            writeByColumnName(filePath, data);
        } catch (Exception e) {
            Log.logErrorMessageAndPrintStackTrace(e);
        }
    }
    private static void writeByColumnName(@NonNull String filePath,
                                          @NonNull List<List<String>> data) throws IOException {

        //int rowNumber=headingsCount;
        XSSFRow row = null;
        XSSFCell cell = null;
        //rowNumber = sheet.getLastRowNum();
        for(List<String> list:data){
            row = sheet.createRow(++rowNumber);
            int columnCount=0;
            for(String s:list){
                cell = row.createCell(columnCount++);
                cell.setCellValue(s);
            }
        }
        FileOutputStream fileOut = new FileOutputStream(filePath);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

}

