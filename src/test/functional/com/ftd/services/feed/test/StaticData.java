package com.ftd.services.feed.test;


public class StaticData {

    public static final String SITE_FTD = "ftd";
    public static final String SITE_PRO = "proflowers";
    public static final int RESP_CODE_200=200;
    //product parameters
    public static final String URL = "url";
    public static final String SKUID = "skuId";
    public static final String TITLE = "title";
    public static final String PRICE = "price";
    public static final String DATE = "date";
    public static final String DESCRIPTION = "description";
    public static final String SITENAME = "siteName";
    public static final String IMAGES = "images";
    public static final String TAGS = "tags";
    public static final String VARS = "vars";
    public static final String NOT_APPLICABLE ="n/a";
    public static final String DEFAULT_PRODUCT_COUNT="defaultProductCount";
    public static final String DEFAULT_FROM_DAYS = "7";
}
