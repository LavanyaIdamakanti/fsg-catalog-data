package com.ftd.services.feed.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.ftd.automation.framework.utilities.Properties;
import com.ftd.services.product.api.domain.response.*;
import org.apache.commons.collections4.CollectionUtils;

import com.ftd.automation.framework.report.Report;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

public class ProductService {

    private Map<String, Map<String, String>> tagsMap = new HashMap<>();

    private Map<String, String> categoriesMap = new HashMap<>();

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * This method makes a call to Product service with given data and returns the response that is received
     *
     * @param productId
     * @param siteID
     * @param prop
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public ProductServiceResponse getProductServiceResponse(String productId, String siteID, Properties prop)
            throws FileNotFoundException, IOException {
        Response response = null;
        String clientContext = prop.getProperty("clientContext");
        String productBaseUri = prop.getProperty("siteUri") + prop.getProperty("Product-ms-URI");
        RequestSpecification request = RestAssured.given().baseUri(productBaseUri)
                .header("clientId", "test")
                .header("Accept", "application/json")
                .header("clientContext", clientContext);
                //.queryParam("sourcePrice", true);
               // .queryParam("include", "categories");
        response = request.get(productId);
        // response.then().log().all();
        response.then().statusCode(StaticData.RESP_CODE_200);
        ProductServiceResponse productResponse = response.getBody().as(ProductServiceResponse.class);
        return productResponse;
    }

    public List<String> getAllProducts(String siteID, Properties prop)
            throws FileNotFoundException, IOException {
        List<String> productList;
        Response response = null;
        String clientContext = prop.getProperty("clientContext");
        String productBaseUri = prop.getProperty("siteUri") + prop.getProperty("Product-ms-URI") + "ids";
        RequestSpecification request = RestAssured.given().baseUri(productBaseUri)
                .header("clientId", "test")
                .header("Accept", "application/json")
                .header("clientContext", clientContext);
        /*request.queryParam("operational.isActive", false)
                .queryParam("operational.isDisplayEligible", false)
                .queryParam("_search.origin", "apollo");*/
        response = request.get();
        // response.then().log().all();
        response.then().statusCode(StaticData.RESP_CODE_200);
        productList = response.getBody().as(List.class);
        return productList;
    }

    public void writeProductData(ProductServiceResponse productResponse, String filePath, String sheetName, int excelHeadingCount){

        List<List<String>> finalList=new ArrayList<>();

        if(CollectionUtils.isNotEmpty(productResponse.getProducts())){
            productResponse.getProducts().forEach(product->{
                List<String> productInfo=new ArrayList<>();
                StringBuilder desc= new StringBuilder();

                productInfo.add(product.getId());
                productInfo.add(product.getName());

                if (product.getPriceAttributes() != null && product.getPriceAttributes().getSourcePrice() != null) {
                    productInfo.add(product.getPriceAttributes().getSourcePrice().toString());
                } else {
                    Report.info("source price is null for product : " + product.getId());
                }

                for(Desc description:product.getDescription()){
                    desc.append(description.getValue());
                }
                productInfo.add(desc.toString());
                if (CollectionUtils.isNotEmpty(product.getProductAttributes())) {
                    for(Attributes productAttribute: product.getProductAttributes()){
                        if(productAttribute.getName().equalsIgnoreCase("productType")){
                            productInfo.add(productAttribute.getValues().get(0).getValue());
                        }
                        if(productAttribute.getName().equalsIgnoreCase("productSubType")){
                            productInfo.add(productAttribute.getValues().get(0).getValue());
                        }
                    }
                }

                List<String> categories;
                if (CollectionUtils.isNotEmpty(product.getCategories())) {
                    categories = product.getCategories().stream()
                            .filter(Objects::nonNull)
                            .flatMap(category -> category.getCategory().stream()).map(specificCategory -> specificCategory.getDisplayName()).collect(Collectors.toSet()).stream().filter(Objects::nonNull).collect(Collectors.toList());
                    String categoriesString= categories.stream().collect(Collectors.joining(","));
                    if (CollectionUtils.isNotEmpty(categories)) {
                        productInfo.add(categoriesString);
                    }
                }else{
                    productInfo.add("NA");
                }

                for(Desc desc1: product.getDescription()){
                    if(desc1.getType().equalsIgnoreCase("details")){
                        String value=desc1.getValue();
                        System.out.println(value);
                        productInfo.add(value);
                    }
                }
                finalList.add(productInfo) ;
            });
        }
        Utils.writeToExcel(filePath,sheetName,finalList );
    }
    public void writeFTDProductData(ProductServiceResponse productResponse, String filePath, String sheetName, int excelHeadingCount){

        List<List<String>> finalList=new ArrayList<>();

        if(CollectionUtils.isNotEmpty(productResponse.getProducts())){
            productResponse.getProducts().forEach(product->{
                List<String> productInfo=new ArrayList<>();

                productInfo.add(product.getId());
                productInfo.add(product.getName());

                if(StringUtils.isNotEmpty(product.getOperational().getIsActive().toString())){
                    productInfo.add(product.getOperational().getIsActive().toString());
                }else{
                    productInfo.add("NA");
                }

                try {
                    //product.getFloristAttributes().getRefNumber()
                    productInfo.add(product.getFloristAttributes().getRefNumber());
                }catch (NullPointerException e){
                    productInfo.add("NA");
                }

                if (CollectionUtils.isNotEmpty(product.getProductAttributes())) {
                    for(Attributes productAttribute: product.getProductAttributes()){
                        if(productAttribute.getName().equalsIgnoreCase("productType")){
                            productInfo.add(productAttribute.getValues().get(0).getValue());
                        }
                        if(productAttribute.getName().equalsIgnoreCase("productSubType")){
                            productInfo.add(productAttribute.getValues().get(0).getValue());
                        }
                    }
                }

                finalList.add(productInfo) ;
            });
        }
        Utils.writeToExcel(filePath,sheetName,finalList );
    }
}
